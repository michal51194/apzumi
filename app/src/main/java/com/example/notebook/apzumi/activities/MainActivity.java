package com.example.notebook.apzumi.activities;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ViewFlipper;

import com.example.notebook.apzumi.R;
import com.example.notebook.apzumi.adapter.RepoAdapter;
import com.example.notebook.apzumi.api.Api;
import com.example.notebook.apzumi.data.RepoDataContainer;
import com.example.notebook.apzumi.github_response.GithubResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private List<RepoDataContainer> repoDataContainerList = new ArrayList<>();
    private RecyclerView.Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final RecyclerView repoList = findViewById(R.id.repos_rv);
        final ViewFlipper viewFlipper = findViewById(R.id.flipper);

        repoList.setHasFixedSize(true);
        repoList.setLayoutManager(new LinearLayoutManager(this));

        Api.Factory.getGithubInstance().getGithubRepos().enqueue(new Callback<List<GithubResponse>>() {
            @Override
            public void onResponse(Call<List<GithubResponse>> call, Response<List<GithubResponse>> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        for (GithubResponse gResponse : response.body()) {
                            RepoDataContainer repoDataContainer = new RepoDataContainer(gResponse.getFullName(),
                                    gResponse.getName(), gResponse.getOwner().getAvatarUrl(), gResponse.getDescription(), false);
                            repoDataContainerList.add(repoDataContainer);
                        }
                    }
                    adapter = new RepoAdapter(MainActivity.this, repoDataContainerList);
                    viewFlipper.showNext();
                    repoList.setAdapter(adapter);

                }
            }

            @Override
            public void onFailure(Call<List<GithubResponse>> call, Throwable t) {
            }
        });

    }

}
