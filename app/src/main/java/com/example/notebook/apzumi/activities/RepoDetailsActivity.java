package com.example.notebook.apzumi.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.notebook.apzumi.R;
import com.example.notebook.apzumi.data.RepoDataContainer;
import com.example.notebook.apzumi.utils.AvatarUtils;

public class RepoDetailsActivity extends FragmentActivity {

    private static final String USERNAME = "USERNAME";
    private static final String REPO_NAME = "REPO_NAME";
    private static final String URL_AVATAR = "URL_AVATAR";
    private static final String DESCRIPTION = "DESCRIPTION";

    public static void start(Context context, RepoDataContainer dataContainer) {

        Intent intent = new Intent(context, RepoDetailsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(USERNAME, dataContainer.getUserName());
        bundle.putString(REPO_NAME, dataContainer.getRepoName());
        bundle.putString(URL_AVATAR, dataContainer.getUrlAvatar());
        bundle.putString(DESCRIPTION, dataContainer.getRepoDescription());
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_activity);

        ImageView avatar = findViewById(R.id.avatar_details_iv);
        TextView username = findViewById(R.id.username_details_tv);
        TextView repoName = findViewById(R.id.reponame_details_tv);
        TextView description = findViewById(R.id.description_tv);

        Bundle bundles = getIntent().getExtras();
        AvatarUtils.setAvataronImageView(bundles.getString(URL_AVATAR), avatar);
        username.setText(bundles.getString(USERNAME));
        repoName.setText(bundles.getString(REPO_NAME));
        description.setText(bundles.getString(DESCRIPTION));
    }
}
