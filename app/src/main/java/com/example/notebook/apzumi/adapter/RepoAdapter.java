package com.example.notebook.apzumi.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.notebook.apzumi.R;
import com.example.notebook.apzumi.activities.RepoDetailsActivity;
import com.example.notebook.apzumi.data.RepoDataContainer;
import com.example.notebook.apzumi.utils.AvatarUtils;

import java.util.List;

public class RepoAdapter extends RecyclerView.Adapter<RepoAdapter.ViewHolder> {

    private List<RepoDataContainer> repoList;
    private Context context;

    public RepoAdapter(Context context, List<RepoDataContainer> repoList) {
        this.context = context;
        this.repoList = repoList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.repo_adapter, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final RepoDataContainer repoData = repoList.get(position);

        AvatarUtils.setAvataronImageView(repoData.getUrlAvatar(), holder.avatar);
        holder.userName.setText(repoData.getUserName());
        holder.repoName.setText(repoData.getRepoName());

        if(repoData.isBitbucketRepo()) {
            holder.layout.setBackgroundColor(Color.BLUE);
        }

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RepoDetailsActivity.start(context, repoData);
            }
        });


    }

    @Override
    public int getItemCount() {
        return repoList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView avatar;
        public TextView userName;
        public TextView repoName;
        public LinearLayout layout;

        public ViewHolder(View itemView) {
            super(itemView);

            layout = itemView.findViewById(R.id.layout_ll) ;
            avatar = itemView.findViewById(R.id.avatar_iv);
            userName = itemView.findViewById(R.id.username_tv);
            repoName = itemView.findViewById(R.id.reponame_tv);

        }
    }
}
