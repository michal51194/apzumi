package com.example.notebook.apzumi.api;

import com.example.notebook.apzumi.bitbucket_response.BitbucketResponse;
import com.example.notebook.apzumi.github_response.GithubResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

public interface Api {

    String BITBUCKET_URL = "https://api.bitbucket.org/";
    String GITHUB_URL = "https://api.github.com/";

    @GET("2.0/repositories?fields=values.name,values.owner,values.description")
    Call<BitbucketResponse> getBitbucketRepos();

    @GET("repositories")
    Call<List<GithubResponse>> getGithubRepos();

    class Factory {
        private static Api service;
        public static Api getBitbucketInstance() {
            if(service == null) {
                Retrofit retrofit = new Retrofit.Builder()
                        .addConverterFactory(GsonConverterFactory.create())
                        .baseUrl(BITBUCKET_URL)
                        .build();

                service = retrofit.create(Api.class);
                return service;
            } else {
                return service;
            }
        }

        public static Api getGithubInstance() {
            if(service == null) {
                Retrofit retrofit = new Retrofit.Builder()
                        .addConverterFactory(GsonConverterFactory.create())
                        .baseUrl(GITHUB_URL)
                        .build();

                service = retrofit.create(Api.class);
                return service;
            } else {
                return service;
            }
        }

    }

}
