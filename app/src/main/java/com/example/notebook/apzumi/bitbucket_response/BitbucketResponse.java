
package com.example.notebook.apzumi.bitbucket_response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BitbucketResponse {

    @SerializedName("values")
    @Expose
    private List<Value> values = null;

    public List<Value> getValues() {
        return values;
    }

    public void setValues(List<Value> values) {
        this.values = values;
    }

}
