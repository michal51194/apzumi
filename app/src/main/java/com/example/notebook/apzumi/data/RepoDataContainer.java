package com.example.notebook.apzumi.data;

public class RepoDataContainer {

    private String repoName;
    private String userName;
    private String urlAvatar;
    private String repoDescription;
    private boolean isBitbucketRepo;

    public RepoDataContainer(String repoName, String userName, String urlAvatar) {
        this.repoName = repoName;
        this.userName = userName;
        this.urlAvatar = urlAvatar;
    }

    public RepoDataContainer(String repoName, String userName, String urlAvatar, String repoDescription, boolean isBitbucketRepo) {
        this.repoName = repoName;
        this.userName = userName;
        this.urlAvatar = urlAvatar;
        this.repoDescription = repoDescription;
        this.isBitbucketRepo = isBitbucketRepo;
    }

    public String getRepoName() {
        return repoName;
    }

    public void setRepoName(String repoName) {
        this.repoName = repoName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUrlAvatar() {
        return urlAvatar;
    }

    public void setUrlAvatar(String urlAvatar) {
        this.urlAvatar = urlAvatar;
    }

    public String getRepoDescription() {
        return repoDescription;
    }

    public void setRepoDescription(String repoDescription) {
        this.repoDescription = repoDescription;
    }

    public boolean isBitbucketRepo() {
        return isBitbucketRepo;
    }

    public void setBitbucketRepo(boolean bitbucketRepo) {
        isBitbucketRepo = bitbucketRepo;
    }
}
