package com.example.notebook.apzumi.utils;

import android.widget.ImageView;
import com.squareup.picasso.Picasso;

public class AvatarUtils {

    public static void setAvataronImageView(String url, ImageView imageView) {
        Picasso.get().load(url).into(imageView);
    }

}
